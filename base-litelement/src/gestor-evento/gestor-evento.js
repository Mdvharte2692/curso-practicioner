import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super()

    }

    processEvent(e) {
        console.log("Process Event");
        console.log(e);

        this.shadowRoot.getElementById("receptorEvento").course = e.detail.course;
        this.shadowRoot.getElementById("receptorEvento").year = e.detail.year;

    }

    render() {
        return html`
            <h1>Gestor evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receptorEvento"></receptor-evento>
        `;
    }

}

customElements.define("gestor-evento", GestorEvento);
//("etiqueta", clase a la que se refiere)