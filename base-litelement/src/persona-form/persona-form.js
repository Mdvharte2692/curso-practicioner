import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: Object,
            editingPerson: Boolean,
        };
    }

    constructor() {
        super()
        this.person = {};
        this.resetFormData();
    }
    
    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad Name con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad Profile con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYears(e){
        console.log("updateYears");
        console.log("Actualizando la propiedad Years con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault(); // Evitamos que haga el submit del formulario por defecto
        this.dispatchEvent(new CustomEvent("persona-form-close", {}))
        this.resetFormData();
    }

    storePerson(e){
        console.log("storePerson");
        console.log("Se va a guardar una persona");
        e.preventDefault(); // Evitamos que haga el submit del formulario por defecto
        
        this.person.photo = {
            src: "../img/photo.jpg"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }))
        // console.log(this.person);
    }

    resetFormData(){
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.editingPerson = false;
    }


    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre Completo</label>
                    <input 
                        class="form-control" 
                        type="text" 
                        placeholder="Nombre Completo"
                        @input="${this.updateName}"
                        .value="${this.person.name}"
                        ?disabled="${this.editingPerson}">
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea 
                        class="form-control" 
                        placeholder="Perfil" 
                        rows="5"
                        @input="${this.updateProfile}"
                        .value=${this.person.profile}></textarea>
                </div>
                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input 
                        class="form-control" 
                        placeholder="Años en la empresa"
                        @input="${this.updateYears}"
                        .value=${this.person.yearsInCompany}></textarea>
                </div>
                <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
            </form>
        </div>

        `;
    }

}

customElements.define("persona-form", PersonaForm);
//("etiqueta", clase a la que se refiere)