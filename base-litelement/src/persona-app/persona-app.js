import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: Array
        };
    }

    constructor() {
        super()

    }

    updated(changedProperties) {
        if (changedProperties.has("people")) {
            console.log("Ha cambiado la propiedad PEOPLE en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    newPerson(e) {
        console.log("newPerson ---> persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeople(e) {
        console.log("updatedPeople ---> persona-app");
        this.people = e.detail.people;
    }

    updatedPeopleStats(e) {
        console.log("updatedPeopleStats ---> persona-app");
        console.log(e.detail);
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;


    }

    newMaxYearsInCompanyFilter(e) {
        console.log("newMaxYearsInCompanyFilter")
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompanyFilter;

    }


    render() {
        return html`
            <div>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
                    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
                <!-- <persona-header></persona-header> -->
                <div class="row">
                    <persona-sidebar class="col-2" @new-person="${this.newPerson}"
                        @update-max-years-filter="${this.newMaxYearsInCompanyFilter}">
                    </persona-sidebar>
                    <persona-main class="col-10" @updated-people="${this.updatedPeople}">
                    </persona-main>
                </div>
                <!-- <persona-footer></persona-footer> -->
                <persona-stats @updated-people-stats="${this.updatedPeopleStats}">
                </persona-stats>
            </div>
        `;
    }

}

customElements.define("persona-app", PersonaApp);
//("etiqueta", clase a la que se refiere)