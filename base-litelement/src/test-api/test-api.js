import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: Array
        };
    }

    constructor() {
        super()
        this.movies = [];
        this.getMovieData();
    }

    getMovieData(){
        console.log("getMovieData");
        let xhr = new XMLHttpRequest(); //Petición AJAX
       
        xhr.onload = () => {  //Ticket
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
                console.log(xhr);

                let APIResponse = JSON.parse(xhr.responseText);
                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/"); //Abre un canal
        xhr.send();
        console.log("Fin de getMovieData");
    }

    render() {
        return html`
            ${this.movies.map(
                movie => html`
                    <div>La película <strong>${movie.title}</strong> fué dirigida por <strong>${movie.director}</strong></div>
                `
            )}
        `;
    }

}

customElements.define("test-api", TestApi);
//("etiqueta", clase a la que se refiere)