import { LitElement, html, css } from 'lit-element';

class TestBootstrap extends LitElement {

    static get styles(){
        return css ` 
            .redbg{
                background-color: red;
            }

            .greenbg{
                background-color: green;
            }

            .bluebg{
                background-color: blue;
            }

            .greybg{
                background-color: grey;
            }
        `;
    }


    static get properties(){
        return {
        
        };
    }

    constructor(){
        super()
 
    }


    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h1>Test bootstrap</h1>
            <div class="row greybg">
                <div class="col-1 col-sm-6 redbg">Col 1</div>
                <div class="col-5 col-sm-1 greenbg">Col 2</div>
                <div class="col-4 col-sm-1 bluebg">Col 3</div>
            </div>
        `;
    }

}

customElements.define("test-bootstrap", TestBootstrap);
//("etiqueta", clase a la que se refiere)