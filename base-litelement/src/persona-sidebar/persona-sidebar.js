import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: Object
        };
    }

    constructor() {
        super();
        this.peopleStats = {};
    }

    newPerson(e) {
        //Recogemos el evento click del button AÑADIR
        console.log("newPerson ---> persona-sidebar");
        console.log("persona sidebar clickado");
        //Disparamos el customEvent
        this.dispatchEvent(new CustomEvent("new-person", {})); //No es necesario pasar nada en el detail
        //Se recoje en el persona-app
    }

    updateMaxYearsInCompanyFilter(e) {
        console.log("updateMaxYearsInCompanyFilter");
        console.log(e.target.value)
        this.dispatchEvent(new CustomEvent("update-max-years-filter", {
            detail: {
                maxYearsInCompanyFilter: e.target.value
            }
        }
        )
        )
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> Personas
                    </div>
                    <div class="mt-5">
                        <button class="w-100 btn bg-success" style="font-size:50px;" @click=${this.newPerson}>
                            <strong>+</strong>
                        </button>
                    </div>
                    <div>
                        Filtrado de personas en base a su antiguedad en la empresa
                        <input type="range" min="0" max="${this.peopleStats.maxYearsInCompany}" step="1"
                            value="${this.peopleStats.maxYearsInCompany}" @input="${this.updateMaxYearsInCompanyFilter}" />
                    </div>
                </section>
            </aside>
        `;
    }

}

customElements.define("persona-sidebar", PersonaSidebar);
//("etiqueta", clase a la que se refiere)