import { LitElement, html } from 'lit-element';

class ReceptorEvento extends LitElement {

    static get properties() {
        return {
            course: String,
            year: String
        };
    }

    constructor() {
        super();

    }




    render() {
        return html`
            <h3>Receptor Evento</h3>
            <h5>Este curso es de ${this.course}</h5>
            <h5>Estamos en el año ${this.year}</h5>

        `;
    }

}

customElements.define("receptor-evento", ReceptorEvento);
//("etiqueta", clase a la que se refiere)